<?php

namespace test\test;

use GuzzleHttp\Client;

class VkParser
{
    const SERVER_PAGE = 'https://api.vk.com';
    const CATEGORY_METHOD_NAME = 'market.getAlbums';
    const PRODUCT_METHOD_NAME = 'market.get';
    const ACCESS_TOKEN = 'xxx';

    /**
     * @var float
     */
    public $apiVersions = 5.52;

    /**
     * @var int
     */
    public $currentCategoryId = -1;

    /**
     * @var string
     */
    public $currentCategoryName = '';

    /**
     * @var bool
     */
    public $calculateEnergy = false;

    /**
     * @var string
     */
    public $patternName = '~none~sui';

    /**
     * @var string
     */
    public $patternCategoryName = '~none~sui';

    /**
     * @var string
     */
    public $patternDescription = '~none~sui';

    /**
     * @var string
     */
    public $patternWeight = '(гр?(амм)?(ов)?\.?|кг)';

    /**
     * @var string
     */
    public $patternSkipWeight = '~none~sui';

    /**
     * @var string
     */
    public $patternVolume = '(литр|л|мл)';

    /**
     * @var string
     */
    public $patternSkipVolume = '~none~sui';

    /**
     * @var string
     */
    public $patternQuantity = '(шт(ук(и|а)?)?\.?|см)';

    /**
     * @var string
     */
    public $patternSkipQuantity = '~none~sui';

    /**
     * @var string
     */
    public $patternEnergy = 'ккал';

    /**
     * @var string
     */
    public $patternSkipEnergy = '~none~sui';

    /**
     * @var array
     */
    private $categories = [];

    /**
     * @var array
     */
    private $products = [];

    /**
     * @var int
     */
    private $shopId = 0;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * VkCommonParser constructor.
     * @param $shopId
     */
    public function __construct($shopId)
    {
        $this->client = new Client();

        $this->shopId = $shopId;
    }

    /**
     * @return array
     * @throws \UnableToBuildMenuException
     */
    public function execute(): array
    {
        $this->parseCategories();
        $this->parseProducts();

        return [$this->categories, $this->products];
    }

    public function parseCategories(): void
    {
        $menuData = $this->receiveCategories();

        foreach ($menuData['response']['items'] as $category) {
            $categoryName = $this->filterCategoryName(remBad($category['title']));

            if (empty($categoryName)) {
                continue;
            }

            $this->categories[$category['id']] = [$categoryName, '', -1];
        }
    }

    /**
     * @throws \UnableToBuildMenuException
     */
    public function parseProducts(): void
    {

        foreach ($this->categories as $categoryId => $category) {
            $productsData = $this->receiveProducts($categoryId);

            if (empty($productsData['response']['items'])) {
                continue;
            }

            $this->currentCategoryId = $categoryId;
            $this->currentCategoryName = $category[0];

            foreach ($productsData['response']['items'] as $product) {
                $this->name = $this->filterName(remBad($product['title']));
                $this->description = $this->filterDescription(remBad($product['description']));;
                $productData = sprintf("%s %s", $this->name, $this->description);

                $weight = $this->pullWeight($productData);
                $volume = $this->pullVolume($productData);
                $energy = $this->pullEnergy($productData);

                if ($this->calculateEnergy) {
                    $energy = $this->calculateEnergy($energy, $weight);
                }

                $this->products[] =
                    [
                        $categoryId,
                        sprintf('%s %s', $this->name, $this->pullQuantity($productData)),
                        $this->description,
                        returnIntPrice($product['price']['text']),
                        htmlspecialchars(trim($product['thumb_photo'])),
                        [],
                        [],
                        'energy' => $energy,
                        'weight' => $weight,
                        'volume' => $volume,
                    ];
            }
        }
    }

    /**
     * @return mixed
     */
    protected function receiveCategories(): array
    {
        $link =
            sprintf(
                '%s/method/%s?v=%s&owner_id=-%s&access_token=%s',
                self::SERVER_PAGE,
                self::CATEGORY_METHOD_NAME,
                $this->apiVersions,
                $this->shopId,
                self::ACCESS_TOKEN
            );

        $response = $this->client->get($link, ['verify' => false, 'stream' => false]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @return mixed
     */
    protected function receiveProducts(int $categoryId): array
    {
        $link =
            sprintf(
                '%s/method/%s?v=%s&owner_id=-%s&access_token=%s&album_id=%s',
                self::SERVER_PAGE,
                self::PRODUCT_METHOD_NAME,
                $this->apiVersions,
                $this->shopId,
                self::ACCESS_TOKEN,
                $categoryId
            );

        sleep(1);
        $response = $this->client->get($link, ['verify' => false, 'stream' => false]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param string $source
     * @return string
     * @throws \UnableToBuildMenuException
     */
    private function pullWeight(string $source): string
    {
        if (preg_match($this->patternSkipWeight, $this->currentCategoryName)) {
            return '';
        }

        $weight = pullPart($source, $this->patternWeight);

        $this->name = str_replace($weight, '', $this->name);
        $this->description = str_replace($weight, '', $this->description);

        if (preg_match('~кг~sui', $weight)) {
            $weight = calculateWeight($weight);
        }

        return $weight;
    }

    /**
     * @param string $source
     * @return string
     * @throws \UnableToBuildMenuException
     */
    private function pullQuantity(string $source): string
    {
        if (preg_match($this->patternSkipQuantity, $this->currentCategoryName)) {
            return '';
        }

        $quantity = pullPart($source, $this->patternQuantity);

        if (preg_match('~\d+\s*(шт(ук(и|а)?)?\.?|см)~sui', $this->name)) {
            return '';
        }

        $this->name = str_replace($quantity, '', $this->name);
        $this->description = str_replace($quantity, '', $this->description);

        return $quantity;
    }

    /**
     * @param string $source
     * @return string
     * @throws \UnableToBuildMenuException
     */
    private function pullVolume(string $source): string
    {
        if (preg_match($this->patternSkipVolume, $this->currentCategoryName)) {
            return '';
        }

        $volume = pullPart($source, $this->patternVolume);

        $this->name = str_replace($volume, '', $this->name);
        $this->description = str_replace($volume, '', $this->description);

        if (!preg_match('~мл~sui', $volume)) {
            $volume = calculateWeight($volume);
        }

        $checkCategoryDrinks = preg_match('~напитки|Алкогол|Безалкогол~sui', $this->currentCategoryName);

        if ($checkCategoryDrinks && empty($volume) && preg_match('~(\d+(\,|\.)\d+)~sui', $this->name, $matches)) {
            $this->name = str_replace($matches[1], '', $this->name);
            $volume = calculateWeight($matches[1]);
        }

        return $volume;
    }

    /**
     * @param string $source
     * @param string $weight
     * @return string
     * @throws \UnableToBuildMenuException
     */
    private function pullEnergy(string $source): string
    {
        if (preg_match($this->patternSkipEnergy, $this->currentCategoryName)) {
            return '';
        }

        $energy = pullPart($source, $this->patternEnergy);

        $this->name = str_replace($energy, '', $this->name);
        $this->description = str_replace($energy, '', $this->description);

        return $energy;
    }

    /**
     * @return string
     */
    private function filterName(string $source): string
    {
        return preg_replace($this->patternName, '',$source);
    }

    /**
     * @return string
     */
    private function filterDescription(string $source): string
    {
        return preg_replace($this->patternDescription, '', $source);
    }

    /**
     * @return string
     */
    private function filterCategoryName(string $categoryName): string
    {
        return preg_replace($this->patternCategoryName, '', $categoryName);
    }
}
